from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer

from sensor_box.models import SensorBox


class SensorBoxSerializer(ModelSerializer):
    class Meta:
        model = SensorBox
        fields = [
            "mac",
            "name",
            "use_system_wifi",
            "ssid",
            "wifi_password",
            "logger_addr",
            "use_dhcp",
            "box_ipaddr",
            "box_network",
            "box_gateway",
            "box_dns1",
            "box_dns2",
            "allow_ota",
        ]

    mac = serializers.ReadOnlyField()

    def validate(self, data):
        this_or_that_validator(
            data.get("use_system_wifi", None),
            data.get("ssid", None),
            data.get("wifi_password", None),
            "Use system wifi or supply ssid and password",
        )
        this_or_that_validator(
            data.get("use_dhcp", None),
            data.get("box_ipaddr", None),
            data.get("box_network", None),
            "Use DHCP or supply ip address and network",
        )
        return data


def this_or_that_validator(boolean_field, other_field1, other_field2, errmsg):
    if not boolean_field and not (other_field1 and other_field2):
        raise ValidationError(errmsg)
