import pytest
from rest_framework.response import Response
from rest_framework.test import APIRequestFactory
from django.test.client import RequestFactory

from sensor_box import models, views


@pytest.mark.django_db
def test_api_create():
    factory = APIRequestFactory()
    request = factory.post(
        "/sensorbox/box/",
        {"name": "deadbeee", "use_system_wifi": True, "use_dhcp": True},
        format="json",
    )
    view = views.SensorBoxViewSet.as_view(actions={"post": "create"})
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_api_create_fail():
    factory = APIRequestFactory()
    request = factory.post("/sensorbox/box/", {"name": "deadbeee", "use_dhcp": True}, format="json")
    view = views.SensorBoxViewSet.as_view(actions={"post": "create"})
    response: Response = view(request)
    print(type(response))
    response.render()
    content_data = response.content.decode()
    assert "non_field_errors" in content_data
    assert response.status_code == 400


@pytest.mark.django_db
def test_api_get_404():
    factory = APIRequestFactory()
    request = factory.get("/sensorbox/box/", format="json")
    view = views.SensorBoxViewSet.as_view(actions={"get": "retrieve"})
    response = view(request, pk="deadbeef")
    assert response.status_code == 404


@pytest.mark.django_db
def test_api_get():
    box = models.SensorBox(mac="deadbeef", name="test box")
    box.save()
    factory = APIRequestFactory()
    request = factory.get("/sensorbox/box/", format="json")
    view = views.SensorBoxViewSet.as_view(actions={"get": "retrieve"})
    response = view(request, pk="deadbeef")
    assert response.status_code == 200


@pytest.mark.django_db
def test_unlog_connect_new():
    boxid = "deadbeef"
    request = RequestFactory().post(f"/sensorbox/unlog/connect/{boxid}")

    views.unlog_connect(request, boxid)
    test_box = models.SensorBox.objects.get(mac=boxid)
    assert test_box.connection_state == models.SensorBox.box_states.USB


@pytest.mark.django_db
def test_unlog_connect_and_disconnect():
    boxid = "deadbeef"
    request = RequestFactory().post(f"/sensorbox/unlog/connect/{boxid}")
    views.unlog_connect(request, boxid)
    views.unlog_disconnect(request)
    test_box = models.SensorBox.objects.get(mac=boxid)
    assert test_box.connection_state == models.SensorBox.box_states.OFFLINE
