# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import SensorBox


@admin.register(SensorBox)
class SensorBoxAdmin(admin.ModelAdmin):
    list_display = (
        "mac",
        "name",
        "use_system_wifi",
        "ssid",
        "wifi_password",
        "use_dhcp",
        "box_ipaddr",
        "box_network",
        "box_gateway",
        "box_dns1",
        "box_dns2",
        "logger_addr",
        "logger_username",
        "logger_password",
        "allow_ota",
        "connection_state",
    )
    list_filter = ("use_system_wifi", "use_dhcp", "connection_state")
    search_fields = ("name",)
