import json

from django.http import Http404, HttpResponse, JsonResponse
from rest_framework import permissions, viewsets

from core.views import lock
from sensor_box.models import SensorBox
from sensor_box.serializers import SensorBoxSerializer

import zmq


class SensorBoxViewSet(viewsets.ModelViewSet):
    queryset = SensorBox.objects.all()
    serializer_class = SensorBoxSerializer
    permission_classes = [permissions.AllowAny]


def flash(request, boxid: str):
    if boxid.lower() == "usb":
        line_to_send = json.dumps({"what": "flash"})
        try:
            lock.acquire()
            context = zmq.Context()
            logger_mq = context.socket(zmq.PUSH)
            logger_mq.connect("tcp://127.0.0.1:5557")
            try:
                logger_mq.send_string(line_to_send, zmq.NOBLOCK)
            except Exception:
                return HttpResponse(status=504)
            context.destroy(500)
        finally:
            lock.release()
    if boxid.lower() == "usb" or SensorBox.objects.get(mac__iexact=boxid):
        return JsonResponse({"Result": "OK"})
    raise Http404


def detect(request):
    usb = SensorBox.objects.filter(connection_state=SensorBox.box_states.USB)
    online = SensorBox.objects.filter(connection_state=SensorBox.box_states.ONLINE)
    offline = SensorBox.objects.filter(connection_state=SensorBox.box_states.OFFLINE)

    return JsonResponse(
        {
            "usb": [sb.as_urldict() for sb in usb],
            "online": [sb.as_urldict() for sb in online],
            "offline": [sb.as_urldict() for sb in offline],
        }
    )


def unlog_connect(request, boxid: str):
    name = request.POST.get("name")
    box, created = SensorBox.objects.get_or_create(mac=boxid)
    box.connection_state = SensorBox.box_states.USB
    if name:
        box.name = name
    box.save()

    if created:
        message = "Record created."
        return_code = 201
    else:
        message = "Record updated."
        return_code = 200

    return JsonResponse({"Result": "OK", "message": message}, status=return_code)


def unlog_disconnect(request):
    boxen = SensorBox.objects.filter(connection_state=SensorBox.box_states.USB).update(
        connection_state=SensorBox.box_states.OFFLINE
    )
    return JsonResponse({"Result": "OK", "message": f"Updated {boxen} entities."})
