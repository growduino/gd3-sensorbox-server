from django.apps import AppConfig


class SensorboxConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "sensor_box"
