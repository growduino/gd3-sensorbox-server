from django.db import models


# Create your models here.
class SensorBox(models.Model):
    class box_states(models.TextChoices):
        USB = "usb", "usb"
        ONLINE = "on", "online"
        OFFLINE = "off", "offline"

    mac = models.CharField(max_length=12, unique=True, primary_key=True)  # plni se samo
    name = models.CharField(max_length=20, unique=True)
    use_system_wifi = models.BooleanField(default=False)
    ssid = models.CharField(max_length=31, blank=True)
    wifi_password = models.CharField(max_length=63, blank=True)
    use_dhcp = models.BooleanField(default=True)
    box_ipaddr = models.GenericIPAddressField(blank=True, null=True)
    box_network = models.GenericIPAddressField(blank=True, null=True)
    box_gateway = models.GenericIPAddressField(blank=True, null=True)
    box_dns1 = models.GenericIPAddressField(blank=True, null=True)
    box_dns2 = models.GenericIPAddressField(blank=True, null=True)
    logger_addr = models.CharField(max_length=128, blank=True, null=True)
    logger_username = models.CharField(max_length=32, blank=True, null=True)
    logger_password = models.CharField(max_length=32, blank=True, null=True)
    allow_ota = models.BooleanField(default=True)
    connection_state = models.CharField(
        max_length=3,
        choices=box_states.choices,
        default=box_states.OFFLINE,
    )

    def as_urldict(self):
        return {"id": self.mac, "name": self.name or self.mac}
