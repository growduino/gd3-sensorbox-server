from django.urls import include, path
from rest_framework import routers

from sensor_box import views

app_name = "sensor_box"


router = routers.DefaultRouter()
router.register(r"box", views.SensorBoxViewSet, basename="box")

urlpatterns = [
    path("detect/", views.detect, name="detect"),
    path("<boxid>/flash", views.flash, name="flash-box"),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("", include(router.urls)),
    path("unlog/connect/<boxid>", views.unlog_connect, name="unlog-connect"),
    path("unlog/disconnect", views.unlog_disconnect, name="unlog-disconnect"),
]
